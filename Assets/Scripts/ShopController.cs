﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    [SerializeField] private List<Item> _items = new List<Item>();
    [SerializeField] private Transform parentItem;
    [SerializeField] private GameObject itemPrefab;

    private List<ItemData> _itemData = new List<ItemData>();

    public static Action<int, int> buyEvent;

    void Start()
    {
        for (int index = 0; index < _items.Count; index++)
        {
            var itemInit = Instantiate(itemPrefab, parentItem.position, Quaternion.identity, parentItem);
            _itemData.Add(itemInit.GetComponent<ItemData>());
            _itemData[index].Init(_items[index], index);

            if (index == 0)
                _itemData[index].isCanUnlock = true;

            itemInit.SetActive(false);
        }

        buyEvent += BuyClicked;
        StatsManager.Instance.coinChange += DataCheck;
        StatsManager.Instance.levelChange += DataCheck;
    }

    private void DataCheck()
    {
        foreach (var item in _itemData)
        {
            if (StatsManager.Instance.CurentLevel >= item._levelUnlock && item.isCanUnlock)
            {
                item.gameObject.SetActive(true);
                item._buyBtn.gameObject.SetActive(true);

                if (StatsManager.Instance.CurentCoin >= item._cost)
                    item._buyBtn.interactable = true;
                else
                    item._buyBtn.interactable = false;
            }
        }
    }

    private void BuyClicked(int cost, int index)
    {
        if (StatsManager.Instance.CurentCoin >= cost)
        {
            StatsManager.Instance.ByItem(cost);
            _itemData[index].ItemWasBuyed();
            if (index++ != _itemData.Count)
                _itemData[index++].isCanUnlock = true;
            DataCheck();
        }
    }
}