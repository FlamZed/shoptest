﻿using UnityEngine;
using TMPro;
using System;

public class StatsManager : MonoBehaviour
{
    private static StatsManager _instance;
    public static StatsManager Instance => _instance;

    #region Coin
    [SerializeField] private TMP_Text _cointText;

    public Action coinChange;

    private int _coin = 0;
    public int CurentCoin
    {
        get
        {
            return _coin;
        }
        private set
        {
            _coin = value;
            if (_coin < 0)
                _coin = 0;
            _cointText.text = _coin.ToString();
            coinChange?.Invoke();
        }
    }
    #endregion

    #region Level
    [SerializeField] private TMP_Text _LevelText;

    private int _level = 0;
    public Action levelChange;

    public int CurentLevel
    {
        get
        {
            return _level;
        }
        private set
        {
            _level = value;
            if (_level < 0)
                _level = 0;
            _LevelText.text = _level.ToString();
            levelChange?.Invoke();
        }
    }

    #endregion

    void Awake()
    {
        _instance = this;
    }

    public void ByItem(int value)
    {
        CurentCoin -= value;
    }

    public void AddCoin(int count)
    {
        CurentCoin += count;
    }

    public void AddLevel(int count)
    {
        CurentLevel += count;
    }
}