﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugController : MonoBehaviour
{
    [SerializeField] private int _coinCount = 5;
    [SerializeField] private int _levelCount = 1;
    public void AddCoin()
    {
        StatsManager.Instance.AddCoin(_coinCount);
    }

    public void AddLevel()
    {
        StatsManager.Instance.AddLevel(_levelCount);
    }
}