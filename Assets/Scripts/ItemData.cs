﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ItemData : MonoBehaviour
{
    [SerializeField] private TMP_Text _itemName;
    public Button _buyBtn;

    [HideInInspector] public bool isCanUnlock = false;

    public int _cost;
    public int _levelUnlock;
    private int _index;

    public void Init(Item item, int index)
    {

        _index = index;
        _itemName.text = item.name;
        _cost = item.cost;
        _levelUnlock = item.levelUnlock;

        _buyBtn.onClick.AddListener(ClickBuyItem);

        _buyBtn.gameObject.SetActive(false);
    }

    private void ClickBuyItem()
    {
        ShopController.buyEvent?.Invoke(_cost, _index);
    }

    public void ItemWasBuyed()
    {
        isCanUnlock = false;
        _buyBtn.gameObject.SetActive(false);
    }
}
