﻿using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Shop/AddItem", order = 1)]
public class Item : ScriptableObject
{
    public int cost;
    public int levelUnlock;
}